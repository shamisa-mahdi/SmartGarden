# Smart Garden (Summer and Fall 2018)
- An IoT based project which was responsible for protecting the flowers in the garden using data gathered from sensors, written in C++ (Arduino), Python (Raspberry Pi), HTML, CSS, JavaScript (Front End), Java (Android)
- Internship, Implementing a smart garden using Raspberry Pi, Arduino (Hardware), HTML, CSS, Javascript
(Front End), and Android
- The Smart Garden project included 18 flower pots which our team was responsible for protecting.
- We measured temperature, humidity, soil moisture, smoke (ppm), the water level in the tank, humidity of the floor and light intensity in the garden; then we monitored them on both an Android application and a Web-based dashboard.
- We controlled the actuators (water pumps, fogger, lights, water tap) based on the values which we measured with our sensors. For Back End, we used Thing talk platform, which is a fork of Thingspeak platform for IoT Back End.